﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3
{
	public class Point
	{
		public float X { get; set; }
		public float Y { get; set; }

		public override string ToString()
		{
			return string.Format($"Your point: ({X}, {Y})");
		}
	}
}
