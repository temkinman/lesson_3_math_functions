﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3
{
	internal class Operations
	{
		public static Dictionary<int, string> operations = new Dictionary<int, string>()
		{
			[1] = "SET_POINT",
			[2] = "TASK_1",
			[3] = "TASK_2",
			[4] = "TASK_3",
			[5] = "TASK_4",
			[6] = "EXIT",
		};
		public const string SET_POINT = "1";
		public const string TASK_1 = "2";
		public const string TASK_2 = "3";
		public const string TASK_3 = "4";
		public const string TASK_4 = "5";
		public const string EXIT = "6";
	}
}
