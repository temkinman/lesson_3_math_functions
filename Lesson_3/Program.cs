﻿using System;
using System.Collections.Generic;

namespace Lesson_3
{
	internal class Program
	{
		private static List<string> _operations = new List<string>(Operations.operations.Values);
		private static Point _point;
		static void Main(string[] args)
		{
			Start();
		}

		private static void Start()
		{
			bool isContinue = true;

			while (isContinue)
			{
				PrintMenu();

				int userChoice = InputMenuChoice() - 1;

				if (_point == null && (userChoice > 0 && userChoice < _operations.Count - 1))
				{
					Console.WriteLine("You have to set your point!");
					GoToMenu();
				}
				else
				{
					switch (_operations[userChoice])
					{
						case "SET_POINT":
							SetPoint();
							break;
						case "TASK_1":  //z = (cos x - cos y)^2 - (sin x - sin y)^2
							Task_1();
							break;
						case "TASK_2":  //z = (sin 4x / (1 + cos 4X)) * (cos 2y / (1 + cos 2y))
							Task_2();
							break;
						case "TASK_3":  //z = ( (3x +2)^2 - 24x )^0.5 / ( 3*(y)^0.5 - 2/ (y)^0.5 )
							Task_3();
							break;
						case "TASK_4":  //z = ( (x-1) * x^0.5 - (y-1) * y^0.5 / ( ( x^3*y )^0.5 + xy + x^2 - x )
							Task_4();
							break;
						case "EXIT":
							Console.WriteLine("Good buy!!!");
							isContinue = false;
							break;
						default:
							Console.WriteLine("Wrong operation.");
							break;
					}
				}
			}
		}

		private static void PrintMenu()
		{
			if (_operations == null || _operations.Count == 0)
			{
				return;
			}

			for (int i = 0; i < _operations.Count; i++)
			{
				Console.WriteLine($"{i + 1}: {_operations[i]}");
			}
		}

		private static void SetPoint()
		{
			_point = new Point();

			Console.Write("Enter X: ");
			_point.X = GetUserInput();

			Console.Write("Enter Y: ");
			_point.Y = GetUserInput();
		}

		#region Task_1
		private static void Task_1()
		{
			//z = (cos x - cos y)^2 - (sin x - sin y)^2

			Console.WriteLine(_point.ToString());

			double statement_1 = Math.Cos(_point.X) - Math.Cos(_point.Y); //cos x - cos y
			double statement_2 = Math.Sin(_point.X) - Math.Sin(_point.Y); //sin x - sin y

			double result = Math.Pow(statement_1, 2) - Math.Pow(statement_2, 2);

			Console.WriteLine($"Task_1: result = {Math.Round(result, 2)}");
			GoToMenu();
		}
		#endregion

		#region Task_2
		private static void Task_2()
		{
			//z = (sin 4x / (1 + cos 4X)) * (cos 2y / (1 + cos 2y))

			Console.WriteLine(_point.ToString());

			double value_1 = Math.Sin(4 * _point.X);
			double value_2 = Math.Cos(2 * _point.Y);

			if (value_1 == 0 || value_2 == 0)
			{
				Console.WriteLine($"Task_2: result = 0");
				GoToMenu();
				return;
			}

			double value_3 = (1 + Math.Cos(4 * _point.X));

			if (value_3 == 0)
			{
				Console.WriteLine("You have to change your point X, because for this value is not correctly! Divide by is zero impossible.");
				GoToMenu();
				return;
			}

			double value_4 = (1 + Math.Cos(2 * _point.Y));

			if (value_4 == 0)
			{
				Console.WriteLine("You have to change your point Y, because for this value is not correctly! Divide by zero is impossible.");
				GoToMenu();
				return;
			}

			double statement_1 = value_1 / value_3; //(sin 4x / (1 + cos 4X))
			double statement_2 = value_2 / value_4; //(cos 2y / (1 + cos 2y))

			double result = statement_1 * statement_2;

			Console.WriteLine($"Task_2: result = {Math.Round(result, 2)}");
			GoToMenu();
		}
		#endregion

		#region Task_3
		private static void Task_3()
		{
			//z = ( (3x +2)^2 - 24x )^0.5 / ( 3*(y)^0.5 - 2/ (y)^0.5 )

			Console.WriteLine(_point.ToString());

			if (_point.Y <= 0)
			{
				Console.WriteLine("You have to change your point Y, because for this task it's not correctly!");
				GoToMenu();
				return;
			}

			double statement_1 = Math.Pow(3 * _point.X + 2, 2) - 24 * _point.X; //(3x +2)^2 - 24x

			if (statement_1 < 0)
			{
				Console.WriteLine("You have to change your X, because this statement (3x +2)^2 - 24x < 0");
				GoToMenu();
				return;
			}

			if (statement_1 == 0)
			{
				Console.WriteLine($"Task_3: result = 0");
				GoToMenu();
				return;
			}

			statement_1 = Math.Pow(statement_1, 0.5); //( (3x +2)^2 - 24x )^0.5

			double statement_2 = 3 * Math.Pow(_point.Y, 0.5); // 3*(y)^0.5 
			double statement_3 = 2 / Math.Pow(_point.Y, 0.5); // 2 / (y)^0.5

			if (statement_2 - statement_3 <= 0.01)
			{
				Console.WriteLine("You have to change your Y, because this statement 3*(y)^0.5 - 2 / (y)^0.5 <= 0");
				GoToMenu();
				return;
			}

			double result = statement_1 / (statement_2 - statement_3);

			Console.WriteLine($"Task_3: result = {Math.Round(result, 2)}");
			GoToMenu();
		}
		#endregion

		#region Task_4
		private static void Task_4()
		{
			//z = ( (x-1) * x^0.5 - (y-1) * y^0.5 / ( ( x^3*y )^0.5 + xy + x^2 - x )

			Console.WriteLine(_point.ToString());

			if (_point.Y < 0)
			{
				Console.WriteLine("You have to change your point Y, because for this negative value is not correctly for y^0.5!");
				GoToMenu();
				return;
			}

			if (_point.X < 0)
			{
				Console.WriteLine("You have to change your point X, because for this negative value is not correctly for x^0.5!");
				GoToMenu();
				return;
			}

			if (_point.X == 0)
			{
				Console.WriteLine("You have to change your X. Divide by zero is impossible.");
				GoToMenu();
				return;
			}

			double statement_1 = Math.Pow(_point.X, 3) * _point.Y; // x^3*y

			if (statement_1 < 0)
			{
				Console.WriteLine("You have to change your X, because this statement x^3*y < 0");
				GoToMenu();
				return;
			}

			double statement_2 = Math.Pow(_point.X, 0.5) * (_point.X - 1); // (x-1) * x^0.5
			double statement_3 = (_point.Y - 1) * Math.Pow(_point.Y, 0.5); // (y-1) * y^0.5 
			double statement_4 = statement_2 - statement_3; // (x-1) * x^0.5 - (y-1) * y^0.5 

			if (statement_4 == 0)
			{
				Console.WriteLine($"Task_4: result = 0");
				GoToMenu();
				return;
			}

			double statement_5 = Math.Pow(statement_1, 0.5) + _point.X * _point.Y + Math.Pow(_point.X, 2) - _point.X; // ( ( x^3*y )^0.5 + xy + x^2 - x )

			if (statement_5 == 0)
			{
				Console.WriteLine("You have to change your X, because this statement ( ( x^3*y )^0.5 + xy + x^2 - x ) = 0. Divide by zero is impossible.");
				GoToMenu();
				return;
			}

			double result = statement_4 / statement_5;

			Console.WriteLine($"Task_4: result = {Math.Round(result, 2)}");
			GoToMenu();
		}
		#endregion

		private static void GoToMenu()
		{
			Console.WriteLine("Press any key to go to menu.");
			Console.ReadKey();
			Console.Clear();
		}

		private static int InputMenuChoice()
		{
			while (true)
			{
				string input = Console.ReadLine().Trim();
				bool success = int.TryParse(input, out int result);

				if (success && (result > 0 && result <= _operations.Count))
				{
					return result;
				}
				else
				{
					Console.WriteLine($"Enter value in this range 1..{_operations.Count}");
				}
			}
		}

		private static float GetUserInput()
		{
			while (true)
			{
				string input = Console.ReadLine()
									  .Trim()
									  .Replace(".", ",");

				bool success = float.TryParse(input, out float result);

				if (success)
				{
					return result;
				}
				else
				{
					Console.Write("Enter number: ");
				}
			}
		}
	}
}
